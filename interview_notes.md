# Interview with developer group

Define available position:

- We show a list of available positions and the applicant uses one

What is our role?

- We as a company want to use it but we also want to be able to sell it later

What kind of company are we?

- We are an outsourcing company in the IT domain

We want to support our HR department, not replace it

Applicant is using the service on the one side to fill in information and we/HR uses the service to find the right applicants to then invite for interviews and managing that

A grading system that weights the different atributes to filter and rate with a score

Choosing certificates from a list (inkludig an upload) and have a field for additional

Goals:

- Decrease HR cost by saving time
- Better candidates chosen so more effective teams
- Spend more time for in person 
- Sell it for profit

Have people be interested in positions and get emails if a position opens

What to do if we don't take someone. Keep the application for later or trash it

- ???

Missed some stuff about the open pool 

Risks?

- Downtime if the system has problems
- Gaming the system
- Overcomplication
- Filtering could be too strong

Missed stuff about users not using our service